﻿using CSharpQuarium.Interfaces;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpQuarium.BLL
{
    class Aquarium : IAquarium
    {
        private Dictionary<string, ILiving> Content = new Dictionary<string, ILiving>();
        private int AlgCount = 1;
        public ILiving this[string key]
        {
            get
            {
                if (Content.TryGetValue(key, out ILiving target))
                {
                    return target;
                }
                throw new Exception("Invalid target, noob!");
            }
        }
        private string dailyLog;
        public string DailyLog
        {
            get
            {
                string temp = dailyLog;
                dailyLog = "";
                return temp;
            }
        }

        int IAquarium.AlgaeCount
        {
            get
            {
                return Content.Values.Where(l => l is Algae).Count();
            }
        }

        int IAquarium.FishCount
        {
            get
            {
                return Content.Values.Where(l => l is Fish).Count();
            }
        }

        public bool Add(ILiving living)
        {
            if (!Content.ContainsValue(living))
            {
                if (living is IAlgae)
                {
                    Content.Add($"Algue {AlgCount.ToString()}", living);
                    AlgCount++;
                    return true;
                }
                else if (living is IFish)
                {
                    Fish temp = (Fish)living;
                    if (Content.ContainsKey(temp.Name))
                    {
                        return false;
                    }
                    Content.Add(temp.Name, living);
                    return true;
                }
                else return false;
            }
            else
            {
                return false;
            }
        }

        public IEnumerator<ILiving> GetEnumerator()
        {
            return Content.Values.GetEnumerator();
        }

        public void PassTurn()
        {
            Random rng = new Random(Guid.NewGuid().GetHashCode());

            List<ILiving> dedFish = new List<ILiving>();
            foreach (ILiving item in Content.Values)
            {
                if (item.HP > 0)
                {
                    if (item is IAlgae)
                    {
                        if (item.HP >= 10)
                        {
                            IAlgae algae = new Algae();
                            this.Add(algae);
                            dailyLog += $"{algae.Display()} s'est multipliée. \n";
                            item.HP /= 2;
                        }
                        else
                        {
                            item.HP++;
                        }

                    }
                    else if (item is IFish)
                    {
                        IEnumerable<ILiving> temp = Content.Values.Where(l => (l.HP > 0) && !(l.Equals(this)));
                        List<ILiving> livList = temp.ToList();
                        ILiving target = livList[rng.Next(livList.Count)];

                        if (item.HP >= 5 && target is IFish)
                        {
                            IFish fish1 = (IFish)item;
                            IFish fish2 = (IFish)target;
                            if (fish1.GetType() == fish2.GetType() && fish1.Gender != fish2.Gender)
                            {
                                fish1.Reproduce(fish2);
                            }
                        }
                        else
                        {
                            if (item is ICarnivore)
                            {
                                if (target is IFish)
                                {
                                    ICarnivore carn = (ICarnivore)item;
                                    carn.Manger((IFish)target);

                                    if (target.HP <= 0)
                                    {
                                        dailyLog += $"{item.Display()} a mangé {target.Display()}. \n";
                                    }
                                    else
                                    {
                                        dailyLog += $"{item.Display()} a croqué {target.Display()}. \n";
                                    }

                                }
                            }
                            else if (item is IHerbivore)
                            {
                                if (target is IAlgae)
                                {
                                    IHerbivore herb = (IHerbivore)item;
                                    herb.Eat((IAlgae)target);

                                    if (target.HP <= 0)
                                    {
                                        dailyLog += $"{item.Display()} a mangé {target.Display()}. \n";
                                    }
                                    else
                                    {
                                        dailyLog += $"{item.Display()} a croqué {target.Display()}. \n";
                                    }

                                }
                            }
                        }
                    }
                }
            }

            IEnumerable<KeyValuePair<string, ILiving>> dedstuff = Content.Where(l => l.Value.HP <= 0);
            foreach (KeyValuePair<string, ILiving> item in dedstuff)
            {
                Content.Remove(item.Key);
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return Content.GetEnumerator();
        }
    }
}
