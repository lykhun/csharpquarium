﻿using CSharpQuarium.Interfaces;
using CSharpQuarium.BLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpQuarium.BLL
{
    class Carnivore : Fish, ICarnivore
    {
        public Carnivore(string name) : base(name)
        {
        }

        public void Manger(IFish fish)
        {
            fish.HP -= 4;
            this.HP += 5;
        }
    }
}
