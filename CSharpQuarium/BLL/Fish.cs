﻿using CSharpQuarium.Enums;
using CSharpQuarium.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace CSharpQuarium.BLL
{
    class Fish : Living, IFish
    {
        public Gender Gender { get; set; }
        public string Name { get; set; }

        public Fish(string name)
        {
            HP = 10;
            Random rng = new Random(Guid.NewGuid().GetHashCode());
            int gendernum = rng.Next(2);
            Gender = (Gender)gendernum;
            Name = name;
        }

        public string Display()
        {
            string bloop = $"bloop bloop je suis {Name} et je suis de type {Gender} (mais en français).";
            return bloop;
        }

        public IFish Reproduce(IFish fish)
        {
            if(GetType() != fish.GetType() || Gender == fish.Gender)
            {
                throw new Exception("blah blah blah");
            }
            Type t = fish.GetType();
            ConstructorInfo ctor = t.GetConstructor(new Type[]{typeof(string)});
            Fish child = (Fish)ctor.Invoke(new object[] { Name + fish.Name });
            return child;
        }
    }
}
