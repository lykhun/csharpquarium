﻿using CSharpQuarium.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpQuarium.BLL
{
    class Algae : Living, IAlgae
    {
        public Algae()
        {
            HP = 5;
        }

        public string Display()
        {
            string bloop = $"Je suis une algue et je touche rien.";
            return bloop;
        }
    }
}
