﻿using CSharpQuarium.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpQuarium.BLL
{
    class Herbivore : Fish, IHerbivore
    {
        public Herbivore(string name) : base(name)
        {
        }

        public void Eat(IAlgae algae)
        {
            algae.HP -= 2;
            this.HP += 3;
        }
    }
}
