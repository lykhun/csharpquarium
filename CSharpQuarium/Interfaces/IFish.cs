﻿using CSharpQuarium.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpQuarium.Interfaces
{
    interface IFish : ILiving
    {
        Gender Gender { get; set; } 
        string Name { get; set; }
        IFish Reproduce(IFish fish);
    }
}
