﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpQuarium.Interfaces
{
    interface ILiving
    {
        int HP { get; set; }
        string Display();
    }
}
