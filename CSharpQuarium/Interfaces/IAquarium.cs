﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpQuarium.Interfaces
{
    interface IAquarium : IEnumerable<ILiving>
    {
        ILiving this[string key] { get; }
        bool Add(ILiving living);
	    int AlgaeCount { get; }
        int FishCount { get; }
        void PassTurn();
        string DailyLog { get; }
    }
}
