using CSharpQuarium.BLL;
﻿using CSharpQuarium.Enums;
using CSharpQuarium.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpQuarium
{
    class Program
    {
        static void Main(string[] args)
        {
            IAquarium aquarium = new Aquarium();
            int nbTour = 1;

            Console.WriteLine("Bonjour, bienvenue dans l'Aquarium Athus 2000");
            ConsoleKey choix = new ConsoleKey();

            // BOUCLE AJOUT
            do
            {
                Console.WriteLine("Que voulez-vous ajouter à l'aquarium? 1 pour Poisson, 2 pour Algue, 3 pour continuer");
                choix = Console.ReadKey().Key;

                switch (choix)
                {
                    case ConsoleKey.NumPad1:
                        ConsoleKey choixEspece = new ConsoleKey();
                        Console.WriteLine("Quelle espèce voulez-vous? 1: Saumon, 2: Thon, 3: Sole, 4: Merou, 5: Poisson Clown, 6: Bar, 7: Carpe ");
                        choixEspece = Console.ReadKey().Key;
                        Console.Clear();

                        Console.WriteLine("Veuillez donner un nom au poisson...");
                        string fishName = Console.ReadLine();

                        switch (choixEspece)
                        {
                            case ConsoleKey.NumPad1:
                                Saumon saumon = new Saumon(fishName);
                                if (aquarium.Add(saumon))
                                {
                                    Console.WriteLine($"{fishName} : {saumon.GetType().Name} a été ajouté.");
                                }
                                else { Console.WriteLine($"Echec de l'ajout de {saumon.GetType().Name}"); }
                                break;
                            case ConsoleKey.NumPad2:
                                Thon thon = new Thon(fishName);
                                if (aquarium.Add(thon))
                                {
                                    Console.WriteLine($"{fishName} : {thon.GetType().Name} a été ajouté.");
                                }
                                else { Console.WriteLine($"Echec de l'ajout de {thon.GetType().Name}"); }
                                break;
                            case ConsoleKey.NumPad3:
                                Sole sole = new Sole(fishName);
                                if (aquarium.Add(sole))
                                {
                                    Console.WriteLine($"{fishName} : {sole.GetType().Name} a été ajouté.");
                                }
                                else { Console.WriteLine($"Echec de l'ajout de {sole.GetType().Name}"); }
                                break;
                            case ConsoleKey.NumPad4:
                                Merou merou = new Merou(fishName);
                                if (aquarium.Add(merou))
                                {
                                    Console.WriteLine($"{fishName} : {merou.GetType().Name} a été ajouté.");
                                }
                                else { Console.WriteLine($"Echec de l'ajout de {merou.GetType().Name}"); }
                                break;
                            case ConsoleKey.NumPad5:
                                PoissonClown poissonClown = new PoissonClown(fishName);
                                if (aquarium.Add(poissonClown))
                                {
                                    Console.WriteLine($"{fishName} : {poissonClown.GetType().Name} a été ajouté.");
                                }
                                else { Console.WriteLine($"Echec de l'ajout de {poissonClown.GetType().Name}"); }
                                break;
                            case ConsoleKey.NumPad6:
                                Bar bar = new Bar(fishName);
                                if (aquarium.Add(bar))
                                {
                                    Console.WriteLine($"{fishName} : {bar.GetType().Name} a été ajouté.");
                                }
                                else { Console.WriteLine($"Echec de l'ajout de {bar.GetType().Name}"); }
                                break;
                            case ConsoleKey.NumPad7:
                                Carpe carpe = new Carpe(fishName);
                                if (aquarium.Add(carpe))
                                {
                                    Console.WriteLine($"{fishName} : {carpe.GetType().Name} a été ajouté.");
                                }
                                else { Console.WriteLine($"Echec de l'ajout de {carpe.GetType().Name}"); }
                                break;
                        }
                        break;
                    case ConsoleKey.NumPad2:
                        Algae algue = new Algae();
                        if (aquarium.Add(algue))
                        {
                            Console.WriteLine("Une algue a été ajoutée.");
                        }
                        else { Console.WriteLine("Echec de l'ajout"); }
                        break;
                    default:
                        Console.WriteLine("Cette touche est incorrecte.");
                        break;
                }
            } while (choix != ConsoleKey.NumPad3);


            ConsoleKey nextTurn = new ConsoleKey();
            // BOUCLE JOURNEE
            do
            {
                Console.WriteLine($"Jour {nbTour}:");
                Console.WriteLine("Cette aquarium contient :");
                foreach (ILiving il in aquarium)
                {
                    if (il is Saumon) Console.ForegroundColor = ConsoleColor.Red;
                    else if (il is Thon) Console.ForegroundColor = ConsoleColor.Gray;
                    else if (il is Sole) Console.ForegroundColor = ConsoleColor.Yellow;
                    else if (il is Merou) Console.ForegroundColor = ConsoleColor.Blue;
                    else if (il is PoissonClown) Console.ForegroundColor = ConsoleColor.Cyan;
                    else if (il is Carpe) Console.ForegroundColor = ConsoleColor.Magenta;
                    else if (il is Bar) Console.ForegroundColor = ConsoleColor.DarkBlue;
                    else if (il is Algae) Console.ForegroundColor = ConsoleColor.Green;

                    Console.WriteLine(il?.Display());
                    Console.ForegroundColor = ConsoleColor.White;
                }
                Console.WriteLine(aquarium.Log());

                Console.WriteLine("1: Passer journée, 2: Ajouter poisson, 3: Quitter");
                nbTour++;
                nextTurn = Console.ReadKey().Key;

            } while (nextTurn != ConsoleKey.NumPad3);
        }
    }
}